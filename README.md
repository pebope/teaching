# Table of Contents
## [Amino acid sequence characterization](./Amino_acid_sequence_characterization/)
## [Evolutionary selection](./Evolutionary_selection/)
