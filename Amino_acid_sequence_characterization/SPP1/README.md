# SPP1 (UniProt id: `P10451`) -- sequence characterization

## Search protein at UniProt knowledgebase
> **About**  
> The aim of the UniProt Knowledgebase is to provide users with a comprehensive, high-quality and freely accessible set of protein sequences annotated with functional information. UniProt integrates, interprets, and standardizes data from multiple selected resources to add biological knowledge and associated metadata to protein records and acts as a central hub from which users can link out to 180 other resources.  
>PMID: [33237286](https://pubmed.ncbi.nlm.nih.gov/33237286/)

Searching [UniProt](https://www.uniprot.org/) with id `P10451` will get you here:
* [P10451 · OSTP_HUMAN](https://www.uniprot.org/uniprotkb/P10451).

Right in the beginning, you can download a summary table of the protein sequence features (👉 Download » GFF).

![](screenshots/uniprot_1.png)

Save the file as:

* [P10451.gff](txt/P10451.gff)

It can be opened in a spreadsheet program, where you can easily filter and annotate its contents:

![](screenshots/Screenshot_2022-10-07_13-45-01.png)

### Determine which isoform to use
Scroll down to [Sequence & Isoforms](https://www.uniprot.org/uniprotkb/P10451/entry#sequences). Unless you are interested in a particular isoform (e.g. longest, shortest, somnething else?), let's consider the **canonical** isoform:
* `P10451-1`

You can download its sequence either from there (👉 Download; or 👉 Copy sequence) or from the drop-down menu where we dowloaded the `P10451.gff` (👉 Download » FASTA (canonical)). Many databases let you input the UniProt sequence id (instead the sequence itself), however, sometimes this **may not work** as you expect  and call another isoform. So we will just be **pasting** the [sequence](txt/P10451-1.fasta):
```
>sp|P10451|OSTP_HUMAN Osteopontin OS=Homo sapiens OX=9606 GN=SPP1 PE=1 SV=1
MRIAVICFCLLGITCAIPVKQADSGSSEEKQLYNKYPDAVATWLNPDPSQKQNLLAPQNA
VSSEETNDFKQETLPSKSNESHDHMDDMDDEDDDDHVDSQDSIDSNDSDDVDDTDDSHQS
DESHHSDESDELVTDFPTDLPATEVFTPVVPTVDTYDGRGDSVVYGLRSKSKKFRRPDIQ
YPDATDEDITSHMESEELNGAYKAIPVAQDLNAPSDWDSRGKDSYETSQLDDQSAETHSH
KQSRLYKRKANDESNEHSDVIDSQELSKVSREFHSHEFHSHEDMLVVDPKSKEEDKHLKF
RISHELDSASSEVN
```

### Protein topology
Before using other tools to predict protein topology, let's check what is reported already at UniProt in the following sections:
* [Family & Domains](https://www.uniprot.org/uniprotkb/P10451/entry#family_and_domains) -- Show features for region, compositional bias, motif.
* [PTM/Processing](https://www.uniprot.org/uniprotkb/P10451/entry#ptm_processing) -- Show features for signal, chain, modified residue, glycosylation.

As a **summary** I would prepare a simple list like this (columns correspond to: start, stop, feature):
```
1	16	Signal_peptide
17	314	Osteopontin
41	290	Disordered
159	161	Cell_attachment_motif

45	74	Polar
85	116	Acidic
117	132	Basic_and_acidic
166	194	Basic_and_acidic
216	290	Basic_and_acidic

24	24	Phosphoserine
...     (snip long list to save space)
311	311	Phosphoserine

134	134	O-linkedGlyc
138	138	O-linkedGlyc
143	143	O-linkedGlyc
147	147	O-linkedGlyc
152	152	O-linkedGlyc
``` 
You can use the `GFF` file as a starting point, then save the list as:
* [uniprot.tsv](txt/uniprot.tsv)

### Take a note of the protein location
Go to [Subcellular Location](https://www.uniprot.org/uniprotkb/P10451/entry#subcellular_location). This information will be useful when we search for short functional motifs. Check _both_ **UniProt Annotation** and **GO Annotation**:

|UniProt Annotation|GO Annotation|
|---|---|
|![i](screenshots/Screenshot_2022-10-06_17-06-13.png)|![i](screenshots/Screenshot_2022-10-06_17-06-37.png)|

Our protein is in the following [locations](txt/location.txt):
- Secreted
- cell projection
- endoplasmic reticulum lumen
- extracellular exosome
- extracellular region
- extracellular space
- Golgi apparatus
- perinuclear region of cytoplasm

## Domains search at S.M.A.R.T.
> **About**  
> SMART (a Simple Modular Architecture Research Tool) allows the identification and annotation of genetically mobile domains and the analysis of domain architectures. More than 500 domain families found in signalling, extracellular and chromatin-associated proteins are detectable. SMART version 9 contains manually curated models for more than 1300 protein domains.  
> PMID: [33104802](https://pubmed.ncbi.nlm.nih.gov/33104802/)

Paste the sequence in the box at [SMART](http://smart.embl-heidelberg.de/). Tick the boxes if you want to search for outliers, domains also at PFAM (another database), signal peptide (is your protein supposed to have one?), internal repeats, then click 👉 Sequence SMART:
* [Outlier homologues](http://smart.embl-heidelberg.de/help/smart_glossary.shtml#outlier) and homologues of known structure (domains that are more difficult to predict)
* PFAM domains (consult additionally with the PFAM database)
* signal peptides (if you are sure the protein has it, e.g. type I transmembrane protein)
* internal repeats

![](screenshots/Screenshot_2022-10-06_17-30-58.png)

Depending on the number of queued jobs, you may have to wait some time. Then you'll be presented with the results:

![](screenshots/text828-3-9-2-6.png)

We identified a signal peptide and an OSTEO domain (click on it for more information), with the same coordinates as those given already at UniProt. However, this is not always the case, so it's a good idea to cosult with SMART -- you may find a bit different domain coordinates or predict domains, not listed at UniProt.

## Search for Short Linear Motifs (SLiMs) at ELM
>**About**  
> The Eukaryotic Linear Motif (ELM) is a computational biology resource for investigating candidate functional sites in eukaryotic proteins. The current ELM release includes 317 motif classes incorporating 3934 individual motif instances manually curated from 3867 scientific publications. Functional sites which fit to the description "linear motif" are currently specified as patterns using Regular Expression rules.  
> PMID: [34718738](https://pubmed.ncbi.nlm.nih.gov/34718738/)

Paste the sequence in the box at [ELM](http://elm.eu.org/). Select cell compartment (what did UniProt say about **Protein location**?); hold `SHIFT` + left click, if you want to select several compartments at once; fill in species name (Homo sapiens); 👉 Submit.

![](screenshots/Screenshot_2022-10-06_18-18-14.png)

The results will give you the list of SLiMs plotted onto the protein topology from SMART (which we just saw). To view the properties of each, mouse over it. An important feature is the **conservation score** (range between 0 - 1, the higher the better):

![](screenshots/Screenshot_2022-10-06_18-38-45.png)

Our [results](txt/elm.txt) are divided into three categories:
1. The SLiMs in the following table are **known instances** annotated from the literature.
    *  [159-161] **LIG_Integrin_RGD_1**. This was already listed on UniProt as **Cell attachment site**.
2. Results of ELM motif search **after globular domain filtering, structural filtering and context filtering**.
    * _None_
3. List of **excluded SLiMs** falling inside SMART/PFAM domains and/or **scoring poorly with the structural filter** (if applicable). Let's list only the ones with a high conservation score:
    * [175-177] CLV_NRD_NRD_1
    * [296-300] CLV_PCSK_SKI1_1
    * [159-161] **LIG_Integrin_RGD_1**
    * [23-26], [307-310] [MOD_GlcNHglycan](http://elm.eu.org/elms/MOD_GlcNHglycan.html)
    * [78-83], [105-110] [MOD_N-GLC_1](http://elm.eu.org/elms/MOD_N-GLC_1.html)
>**Note**  
> The known motif **LIG_Integrin_RGD_1** from (1) is listed among the excluded in (3), due to ELM's automatic filterings. For a details, check the [Help page](http://elm.eu.org/infos/help.html): *Why is the context of a functional site important?* and *What are the currently implemented context filters?*. Therefore, you can take the results with a grain of salt: if a motif has a good conservation score and it **makes sense** to you (you're the expert), you may **carefully consider it**. For example, if a crystal structure exists, it is worth checking where it is (surface).

## Predict glycosylation
>**About**  
>The NetNglyc server predicts N-Glycosylation sites in human proteins using artificial neural networks that examine the sequence context of Asn-Xaa-Ser/Thr sequons. The NetOglyc server produces neural network predictions of mucin type GalNAc O-glycosylation sites in mammalian proteins.  
> PMID: [11928486](https://pubmed.ncbi.nlm.nih.gov/11928486/), [23584533](https://pubmed.ncbi.nlm.nih.gov/23584533/)

Search at [NetNGlyc](https://services.healthtech.dtu.dk/service.php?NetNGlyc-1.0) for N-glycosylation sites. Paste the sequence in the box and  and tick if you want to have graphics; 👉 Send file:

![](screenshots/Screenshot_2022-10-07_10-20-13.png)

We have two [sites detected](txt/n_glyc.tsv), both of which correspond to MOD_N-GLC_1 predicted by ELM above:
```
(Threshold=0.5)
----------------------------------------------------------------------
SeqName      Position  Potential   Jury    N-Glyc   agreement result
----------------------------------------------------------------------
sp_P10451_OSTP_HUMAN   79 NESH   0.5493     (6/9)   +     
sp_P10451_OSTP_HUMAN  106 NDSD   0.5817     (6/9)   +     
----------------------------------------------------------------------
```

Search at [NetOGlyc](https://services.healthtech.dtu.dk/service.php?NetOGlyc-4.0) for O-glycosylation sites. Paste the sequence as above. The results are given as a tab separated value (tsv) table, let's save it as:

* [o_glyc.tsv](txt/o_glyc.tsv).

The experimentally determined sites listed at UniProt are there:
```
start	score
134 0.628841
138 0.392174 (below threshold)
143 0.551218
147 0.814851
152 0.452333 (below threshold)
```

* From the predicted-only sites, let's consider those with a very high score (> 0.8):
```
120 0.810969
243 0.896639
```

## Predict Ser, Thr and Tyr phosphorylation sites
>**About**  
>The NetPhos server predicts serine, threonine or tyrosine phosphorylation sites in eukaryotic proteins using ensembles of neural networks.  
>PMID: [10600390](https://pubmed.ncbi.nlm.nih.gov/10600390/)

Search at [NetPhos](https://services.healthtech.dtu.dk/service.php?NetPhos-3.1) for phosphorylation sites. Paste the sequence, residues to predict: all three. UniProt reported the protein as having numerous phosphorylation sites, so tick **For each residue display only the best prediction** to get a more concise list if you want a full list, then do not tick it); 👉 Submit:


![](screenshots/Screenshot_2022-10-07_10-51-00.png)

The results show numerous sites, as a [table](txt/phosph.tsv), sequence position and a graph:
```
MRIAVICFCLLGITCAIPVKQADSGSSEEKQLYNKYPDAVATWLNPDPSQ   #     50
KQNLLAPQNAVSSEETNDFKQETLPSKSNESHDHMDDMDDEDDDDHVDSQ   #    100
DSIDSNDSDDVDDTDDSHQSDESHHSDESDELVTDFPTDLPATEVFTPVV   #    150
PTVDTYDGRGDSVVYGLRSKSKKFRRPDIQYPDATDEDITSHMESEELNG   #    200
AYKAIPVAQDLNAPSDWDSRGKDSYETSQLDDQSAETHSHKQSRLYKRKA   #    250
NDESNEHSDVIDSQELSKVSREFHSHEFHSHEDMLVVDPKSKEEDKHLKF   #    300
RISHELDSASSEVN                                       #    350

.......................S.SS.....Y...............S.   #     50
...........SS..T......T....S..S.................S.   #    100
.S..S..S.....T..S..S..S..S..S....T...T........T...   #    150
....T......S..Y...S.S.........Y........TS...S.....   #    200
.Y............S...S....SY..S........T.S...........   #    250
...S...S....S......S....S....S..........S.........   #    300
..S....S.SS...
```

![](screenshots/netphos-3.1b.sp_P10451_OSTP_HUMAN.gif)

It's a good idea to cross-reference with the information from UniProt. Checking in a spreadsheet, it seems all UniProt listed phosphorylation sites were also reported by NetPhos. From the predicted novel sites, let's consider the ones with very high score (> 0.8):
```
155 T 0.841
162 S 0.864
169 S 0.805
171 S 0.996
```

## Predict Disordered region binding sites (DRBS)
>**About**  
>Intrinsically disordered proteins and protein regions exist without a single well-defined conformation. They carry out important biological functions with multifaceted roles which is also reflected in their evolutionary behavior. IUPred3 is a combined web interface that allows to identify disordered protein regions using IUPred2 and disordered binding regions using ANCHOR2  
>PMID: [34048569](https://pubmed.ncbi.nlm.nih.gov/34048569/)

Paste the sequence at [IUPred3/ANCHOR2](https://iupred.elte.hu/) and select **ANCHOR2**, then click 👉 Submit:

![](screenshots/Screenshot_2022-10-07_11-18-44.png)

The results will show both the IUPred3 (disordered regions) and ANCHOR2 (disordered region binding sites):

![](screenshots/path830-7-5.png)

Download the scores as a table:
* [iupred.tsv](txt/iupred.tsv)

I would consider regions where both IUPred3 and Anchor2 scores are high, e.g. above 0.8. Therefore sort by columns and remove rows with scores below 0.8:

* [iupred_anchor.tsv](txt/iupred_anchor.tsv)

Which corresponds to these regions:
```
53  59  DRBS
67  119 DRBS
125 134 DRBS
```
## Figure
We have summarized everything by now in a single file:
* [summary.tsv](txt/summary.tsv)

Let's turn the coordinates into a figure.

### MyDomains
A tool that is simple to use is [ProSites MyDomains](https://prosite.expasy.org/mydomains):

![](screenshots/Screenshot_2022-10-07_16-45-25.png)

There, I used only the following features as an input (domain, cell adhestion motif, O and N glycosylation sites, DRBS):
```
16, 314, 3, 2, Osteo
160, 1, CAM
134, 0, O
138, 0, O
143, 0, O
147, 0, O
152, 0, O
120, 0, O
243, 0, O
79, 0, N
106, 0, N
53, 59, 1
67, 119, 1
125, 134, 1
```
Which generates this figure:

![](screenshots/Screenshot_2022-10-07_16-47-47.png)

>**Note**  
> The limitation with MyDomains is that if you want to generate a figure which has a variety of motifs, sites and other markups, it is not flexible enough. However, it is excellent for a relatively simple figure, and there's nothing wrong with using it!

### PFAM Custom Domains generator
PFAM will be [retired in January 2023](https://xfam.wordpress.com/2022/08/04/pfam-website-decommission/), so the site has been moved to legacy. The [custom domain generator](http://pfam-legacy.xfam.org/generate_graphic/) uses JSON syntax, described in details in their [guide to graphics Help page](http://pfam-legacy.xfam.org/help#tabview=tab10). The syntax may seem complicated at first, but let's consider a simplified example. This is the code needed to create a 314 amino acids long polypeptide chain (`length`) with a single Osteo domain (`regions` section):
```
{ 
  "length" : "314",  
  "regions" : [  
    {  
      "text" : "Osteo",  
      "colour" : "#24E400",  
      "startStyle" : "curved", 
      "endStyle" : "straight", 
      "start" : "17", 
      "end" : "314",  
    }, 
  ] 
}
```

To add a motif, such as CAM (cell adhesion motif, we had it in ELM), we need a `motifs` section with the motif itself. To add a markup, such as an O-glycosylation site, we need it to be in a `markup` section. Combinig all these together, will produce:

```
{ 
  "length" : "314",  
  "regions" : [  
    {  
      "text" : "Osteo",  
      "colour" : "#24E400",  
      "startStyle" : "curved", 
      "endStyle" : "straight", 
      "start" : "17", 
      "end" : "314",  
    }, 
  ],
   "motifs" : [ 
    { 
      "type" : "cam", 
      "colour" : "#FFD300", 
      "start" : "159", 
      "end" : "161",
    },
  ],
    "markups" : [
    {
      "lineColour" : "#666666",
      "colour" : "#A633FF",
      "v_align" : "top",
      "headStyle" : "line",
      "type" : "O-glyc",
      "start" : "134"
    },
  ],
}
```

Which will generate:

![](screenshots/pfam3.png)

Assembling all parts together from our summary, we can end up with a quite long JSON file. However, once you have the motifs/markups templates, it's a matter of simple copy/paste + modifying the coordinates.
* here's our final file: [spp1.json](txt/spp1.json)
* pasting it [PFAM Domain generator](http://pfam-legacy.xfam.org/generate_graphic/) will get us:

![](screenshots/spp1.png)

It may look quite "busy", so you can leave just the features you are most interested in. Check the code to see what is what!

## Assignment example
* Save the results of all database searches. Data format is up to you (txt, tsv, html, pictures,...).
* Include a file that lists all steps, pointing to their results. Something like [this](txt/assignment.md) is the minimum. File format is up to you (txt, docx, ppt,...).
* Generate a figure from the data you collected.