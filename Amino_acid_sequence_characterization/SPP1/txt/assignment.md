# SPP1 (UniProt id: P10451)

## UniProt
- url: https://www.uniprot.org/uniprotkb/P10451
- overview file: P10451.gff
- canonical isoform: "P10451-1"
- fasta sequence file: P10451-1.fasta
- Uniprot summary file: uniprot.tsv
- cell location: location.txt

## SMART
- summary file: smart.tsv

## ELM
- search at locations: extracellular, Golgi, ER
- motifs summary: elm.txt

## Glycosylation
- n: n_glyc.tsv
- o: o_glyc.tsv

high score:
  - 120 0.810969
  - 243 0.896639

## Phosphorylation
- T,S,Y sites: phosph.tsv

## IUPred3/Anchor2
- raw results: iupred.txt
- comparison: iupred_anchor.tsv

## Final Summary:
- summary.tsv

## Figure
- domains coordinates, e.g. JSON format for PFAM
