# Amino acid sequence characterization
We'll use a single protein, SPP1, for the teaching materials/tutorial. **Materials are in folder:** [SPP1](./SPP1/)

## Introduction
When studying a protein, characterization based on the amino acid sequence, is an important first step in any analysis. Using different online resources (Figure 1), we will look for the following features which collectively show the protein *topology*:
* **protein domains**: relatively long (typically >50 amino acids) regions of the polypeptide chain with a specifically folded structure. Different types of domains have different functions, and identifying the domain type can point us towards the function of the protein.
* **short linear motifs**: short regions of the polypeptide chain, that form compact functional motifs. They have different roles and often represent protein-interaction sites. 
* **glycosylation**: attachment of sugars to secreted or extracellular protein regions. It has a role for protein conformation, stability and adhesion between cells.
* **phosphorylation sites**: the addition of phosphate groups to Serine, Threonine or Tyrosine. It affects protein stability, location, interactions with other proteins and anzymatic activity.
* **disordered regions**: regions that lack a well defined conformation. Typically, they are flexible regions between domains and mediate interactions htrough short linear motifs.

```mermaid
flowchart LR;
style Protein fill:#FCF4E4
style UniProt fill:#FCF4E4
style is fill:#FCF4E4
style localization fill:#FCF4E4
style pt fill:#FCF4E4

style S.M.A.R.T. fill:#FCE4FC
style domains fill:#FCE4FC

style ELM fill:#FCE7E4
style sfm fill:#FCE7E4

style DTU.dk fill:#E4F1FC
style glycosylation fill:#E4F1FC
style gl fill:#E4F1FC
style gl fill:#E4F1FC
style NetPhos fill:#E4F1FC
style phosphorylation fill:#E4F1FC

style iu fill:#E4FCE7
style drbs fill:#E4FCE7

Protein --> UniProt((UniProt KB)) --- is([canonical isoform]) -.- pt(protein topology)
UniProt -..- localization
is --> S.M.A.R.T.((S.M.A.R.T.)) --- domains
is --> ELM((ELM)) --- sfm(short linear motifs)
is --> DTU.dk((DTU.dk))
DTU.dk --- gl(NetNGlyc & NetOGlyc) -.- glycosylation
DTU.dk --- NetPhos -.- phosphorylation
is --> iu((IUPred3)) --- drbs(Disordered regions binding sites)
localization -.- ELM
%%pt & domains & sfm & glycosylation & phosphorylation & drbs -.-> fig((Scheme))
```
>**Figure 1. Sequence characterization workflow.** We will start by a protein identifier (id) and extract information already available at the UniProt knowledgebase (protein topology, cellular localization). There, we'll choose what transcript (isoform) to use (usually that's the *canonical* isoform) for the subsequent analyzes. We'll consult the S.M.A.R.T. database for protein domains and ELM (specifying the protein localization) -- for short linear motifs. Then, we'll check the DTU.dk server for post translational modification sites, such as glycosylation and phosphorylation. Finally, we will predict binding sites within disordered regions at IUPred3.

## Required software
Almost all tools are available online. What you need to have installed locally is:
* A text editor. That's a matter of choice, really. I hear many people use Notepad++ on Windows.
* A spreadsheet program. Most typically that's MS Excel. Free alternatives to MS Office include [LibreOffice](https://www.libreoffice.org/) and [WPS Office](https://www.wps.com/) (free, but closed source).

## Assignment
When you have reviewed all steps, you should conduct an analogous analysis using another protein. Best would be if you use **a protein of your choice**, something you are interested in. Otherwise, you can pick one of the following: `P15391` (CD19_HUMAN), `P61006` (RAB8A_HUMAN). Please, prepare a short report of your workflow with the "raw-data" file(s) from each step (there is an example in the end of the tutorial).
