# Evolutionary selection
We'll use a single protein, SPP1, for the teaching materials/tutorial. **Materials are in folder:** [SPP1_selection](./SPP1_selection/)

## Introduction
The evolutionary history of a gene can reveal functional and structural constraints of the protein it codes, as well as signs of molecular adaptation. The predominant type of natural selection on genes is negative (purifying), which eliminates damaging mutations, while the much more rare, positive (Darwinian) selection, **favors mutations** that are advantageous for the organism.  

A powerful approach to determine molecular evolution derives from comparison of the relative rates of *synonimous* and **non-synonimous substitutions** across species. *Synonimous* (S) mutations *do not change* the amino acid sequence, hence their substitution rate (ΔS) is neutral with respect to selective pressure on the protein product of the gene. **Non-synonimous** (N) mutations **do change** the amino acid sequence, so their substitution rate (ΔN) is a function of selective pressure.

Two widely adopted software suites to investigate molecular evolution are: PAML and HyPhy. Here, we'll use the latter, via its web interface. If you are interested to try PAML, drop me an email. These programs require as an input (Figure 1) two things:
1. **Codon**-aligned orthologues of your gene of interest.
2. A phylogenetic **tree** of the species from which your orthologues are.

|1. Codon aligned orthologues|2. Phylogenetic tree|
|---|---|
|![](SPP1_selection/screenshots/msa.png)|![](SPP1_selection/screenshots/tre.png)|

> **Figure 1. HyPhy prerequisites.** In (1), columns corresponding to individual codons are referred to as **sites**. Example synonymous and non-synonimous codon substitutions are indicated. Species (sequences) represent the outer **branches** of the phylogenetic tree (2).

A major challenge in this type of work is the reliable identification of orthologues. Ideally, sequences should be manually curated, to make sure that they are complete, of good quality (no missing nucleotides) and of the correct isoform. The latter is done, for exmaple, by inspecting their intron/exon organisation. However, this is very time consuming. Therefore, we will use an automated approach, followed by several steps to remove unreliable or too divergent sequences (Figure 2).

```mermaid
flowchart LR;
style Protein fill:#FCF4E4
style is fill:#FCF4E4
style gt fill:#DAFFBE
style Ensembl fill:#DAFFBE
style orthologues fill:#BEFFE4
style CDS fill:#BEE6FF
style Guidance fill:#BEE6FF
style translated fill:#BEE6FF
style MSA fill:#BEE6FF
style Mumsa fill:#BEE6FF
style codon fill:#BEE6FF
style TimeTree fill:#FFCABE
style Tree fill:#FFCABE
style HyPhy fill:#FFBEF3
style FUBAR fill:#FFBEF3
style MEME fill:#FFBEF3

Protein --> is([canonical isoform]) --> Ensembl((Ensembl)) --> orthologues([orthologues]) --> codon(codon MSA) --> HyPhy((HyPhy)) --- FUBAR & MEME

gt((gene tree)) -.- Ensembl
orthologues --> TimeTree((TimeTree)) --> Tree --> HyPhy
orthologues -.-> CDS -.-> translated -.-> MSA -. best .-> orthologues
CDS -.- Guidance((Guidance))
MSA -.- Mumsa((Mumsa))
```
>**Figure 2. Preparation for selection tests workflow.** For our protein of interest, we will first search Ensembl's gene tree for orthologues (canonical isoform). We will download the coding DNA sequences (CDS) of the orthologues and conduct a series of assessments. We will eliminate "bad sequences": too short or with large chunks of unidentified nucleotides. Then, we'll use `Guidance2` to exclude sequences that are too divergent, because they can seriously affect the alignment process in the next step. This can be done already on codon level. Then, we'll translate to amino acids and do multiple sequence alignments (MSA) on protein level. We will try several different programs online (such as `ClustalO`, `MAFFT`, `Muscle`, `PRANK` and `T-coffee`), then score their produced alignments by a program called `Mumsa`. In the end, we will come back to the *orthologues* step, but this time we will have a list of "reliable" orthologues that have passed the `Guidance2` filtering, and for which we have a good quality MSA on the protein level. Then we'll create a codon alignment from the protein MSA and the CDS; we will also obtain a phylogenetic tree for the species in the MSA. Submitting the alignment and tree to HyPhy, we will test for selection by two methods: FUBAR and MEME.

Once orthologues are identified and aligned, there are three main approaches (models) to infer selection:
1. Branches
2. Sites (pervasive selection)
3. Branches-sites (episodic selection) 

The *branches* approach can tell us in which species we detected positive selection for our gene, but cannot tell us which codons are under selection. To detect positive selection for branches, it should be really strong across most of the sites (codons of the gene), something that rarely happens. In this course, we will not use the branches method. The *sites* approach can tell us which codons are under selection (positive or negative), but cannot tell us in which species. It assumes that the selection is constant across branches. We will use FUBAR method from HyPhy to detect such pervasive selection on sites. The *branches-sites* approach can tell us which codons are under positive selection and the proportion of the phylogeny where said selection is detected. It, however, will not tell us exactly which species are affected. This is a very powerful approach and here we will use MEME from HyPhy to detect such episodic selection.

## Example reads
Studies of molecular evolution have successfully pointed to novel regulatory and functional features of different proteins. For example, it has been shown that specific residues enhance protein function, as is the case with MARCO receptor ([Novakowski et al, 2018](https://pubmed.ncbi.nlm.nih.gov/29165618)); that a patch of amino acids have evolved to promote receptor clustering via its transmembrane region, in the case of MER tyrosine kinase ([Evans et al, 2017](https://pubmed.ncbi.nlm.nih.gov/28369510/)); or that several key sites have been evolving in a host-pathogen ‘arms-race’, like in the case of NPC1 cellular receptor and its interaction with Ebola virus ([Pontremoli et al, 2016](https://pubmed.ncbi.nlm.nih.gov/27512112/)).

## Required software
Almost all tools are available online. What you need to have installed locally is:
* A text editor. That's a matter of choice, really. I hear many people use Notepad++ on Windows.
* A spreadsheet program. Most typically that's MS Excel. Free alternatives to MS Office include [LibreOffice](https://www.libreoffice.org/) and [WPS Office](https://www.wps.com/) (free, but closed source).
* A sequence editor. I suggest [AliView](https://ormbunkar.se/aliview/), unless you have some other preferences.
* In addition, it is highly recommended that you have taken the *Amino acid sequence characterization* course.

## Assignment
When you have reviewed all steps, you should conduct an analogous analysis using another protein. Best would be if you use **a protein of your choice**, something you are interested in. Otherwise, you can pick one of the following: `P15391` (CD19_HUMAN), `P61006` (RAB8A_HUMAN). Please, prepare a short report of your workflow with the "raw-data" file(s) from each step (there is an example in the end of the tutorial).
