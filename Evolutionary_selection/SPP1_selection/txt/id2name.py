import re

species = open("ensembl_timetree.tsv").readlines()
sequences = open("cds.095.noDup.fasta").readlines()
output = open("cds.095.names.fasta", "w")
species_list = open("species.095.txt", "w")

species_dict = {}

for line in species:
	contents = line.split("\t")
	species_dict[contents[0]] = contents[1]
	
for row in sequences:
	i = re.search(r"ENS....?P", row)
	if i:
		ambig = i.group()
		output.write(">" + species_dict[ambig[:-1]])
		species_list.write(species_dict[ambig[:-1]])
	else:
		output.write(row)
