# SPP1 (UniProt id: P10451)

## Ensembl
- url: https://www.ensembl.org/Homo_sapiens/Gene/Summary?db=core;g=ENSG00000118785;r=4:87975667-87983532
- Ensembl Gene ID: ENSG00000118785
- Ensembl transcript ID: ENST00000395080.8
- Ensembl protein ID: ENSP00000378517.3

### gene tree:
- SPP1_homologue_sequences.fa
- cleaned: SPP1_homologue_sequences.clean_1.fasta

## Guidance
- Seqs.Orig_DNA.fas.FIXED.Without_low_SP_Seq.0.951.With_Names.fasta
- other results?

## Seq. manipulation
- no duplicates: cds.095.noDup.fasta
- species names: cds.095.noDup.names.fasta
- translated: cds.095.noDup.names.translated.fasta

## TimeTree
- species searched: species.095.noDup.txt
- species tree: species.095.noDup.nwk.
- any peculiarities..?

## MSA
where did you make the MSA? where are the results saved?
- clustalO: clustalo.fasta
- mafft: mafft.fasta
- t-coffee: tcoffee.fasta
- muscle: muscle.fasta
- prank: prank.fasta

## Mumsa
Alignment difficulty (AOS score): 0.786638

Alignment : MOS score
clustalo.fasta : 0.982219
mafft.fasta : 0.983995
muscle.fasta : 0.983188
prank.fasta : 0.981930
tcoffee.fasta : 0.985172

- which one did you choose? why?

## pal2nal
- input 1: tcoffee.fasta
- input 2: cds.095.noDup.names.fasta
- codon alignment: tcoffee.codon.fasta

## HyPhy
- input: tcoffee.codon+tree.hyphy
- FUBAR: fubar.json, fubar.csv
- MEME: meme.json, meme.csv

## Analysis
- reference gene codon coordinates:
  - fubar.hsap.csv, meme.hsap.csv
- reference gene codon coordinates (significant results):
  - fubar.hsap.significant.csv, meme.hsap.significant.csv

## Plot selected sites onto the protein topology
